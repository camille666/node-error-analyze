// 读取文件
const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
// souceMap处理文件
const SourceMapConsumer = require('source-map').SourceMapConsumer;
// 启动构建进程（已构建则不需要）
// const [lineno, columnno, fileName] = process.argv.slice(2); // 第一个参数为行数

async function getInfo(fileName, lineno, columnno) {
    const filePath = path.resolve(__dirname, `./../webApp/${fileName}.map`);

    if (!fs.existsSync(filePath)) {
        return `${filePath} 文件不存在`
    }
    if (!fs.statSync(filePath).isFile) {
        return `${filePath} 不是一个文件`
    }
    // 读取错误文件的map文件
    const consumer = await new SourceMapConsumer(fs.readFileSync(filePath, 'utf8'));
    // 输出map的错误信息
    return consumer.originalPositionFor({
        line: +lineno, // +是为了转化为数字
        column: +columnno
    });
}

function parse(str) {
    const [fileName] = str.match(/dist\/[^ "]+.js/) || str.match(/app\/[^ "]+.js/) || [];
    let [, lineno] = str.match(/lineno[^\d]*(\d+)/) || [];
    let [, colno] = str.match(/colno[^\d]*(\d+)/) || [];
    if (/\.js:\d+:\d+/.test(str)) {
        [, lineno, colno] = str.match(/\.js:(\d+):(\d+)/) || [];
    }

    return [fileName, lineno, colno];
}

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true }));

app.all('/getErrorPos', async (req, res) => {
    const { errorStack = req.query.errorStack } = req.body;
    try {
        if (errorStack) {
            const data = await getInfo(...parse(errorStack))
            return res.json({
                code: 0,
                data,
            });
        }
    } catch (err) {
        return res.json({
            code: 1,
            err: err.message,
            stack: err.stack,
        });
    }
    return res.json({
        code: 1,
    })
});

app.all('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, './index.html'));
});

app.listen('9876', () => {
    console.log('http://localhost:9876/form');
});
